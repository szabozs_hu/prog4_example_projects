﻿CREATE TABLE [dbo].[Brands] (
    [Id]      INT           NOT NULL,
    [Name]    NVARCHAR (50) NOT NULL,
    [Country] NVARCHAR (50) NOT NULL,
    [URL]     NVARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


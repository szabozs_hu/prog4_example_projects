﻿CREATE TABLE [dbo].[ModelsExtras] (
    [ModelId] INT NOT NULL,
    [ExtraId]     INT NOT NULL,
    CONSTRAINT [PK_ModelsExtras] PRIMARY KEY CLUSTERED ([ModelId] ASC, [ExtraId] ASC),
    CONSTRAINT [FK_ModelsExtras_ToModels] FOREIGN KEY ([ModelId]) REFERENCES [dbo].[Models] ([Id]),
    CONSTRAINT [FK_ModelsExtras_ToExtras] FOREIGN KEY ([ExtraId]) REFERENCES [dbo].[Extras] ([Id])
);

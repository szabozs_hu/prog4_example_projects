﻿CREATE TABLE [dbo].[Extras] (
    [Id]    INT           NOT NULL,
    [Name]  NVARCHAR (100) NOT NULL,
    [Price] MONEY         NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


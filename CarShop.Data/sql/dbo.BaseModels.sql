﻿CREATE TABLE [dbo].[BaseModels] (
    [Id]        INT           NOT NULL,
    [Name]      NVARCHAR (50) NOT NULL,
    [BrandId]   INT           NOT NULL,
    [Price] MONEY         NOT NULL,
    [Description] NVARCHAR(MAX) NULL, 
    [PictureURL] NVARCHAR(MAX) NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_BaseModels_ToBrands] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brands] ([Id])
);


﻿using CarShop.Data.Interfaces;
using System.Linq;

namespace CarShop.Data.Repositories
{
    /// <summary>
    /// Repository of brands
    /// </summary>
    public class BrandRepository : IRepository<Brand>
    {
        private CarShopDBEntities entities;

        /// <summary>
        /// Creates the repository.
        /// </summary>
        public BrandRepository(CarShopDBEntities entities)
        {
            this.entities = entities;
        }

        public IQueryable<Brand> GetAll()
        {
            return entities.Brands;
        }
    }
}

﻿using CarShop.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarShop.Data.Repositories
{
    /// <summary>
    /// Repository of models 
    /// </summary>
    public class ModelRepository : IModelRepository
    {
        private CarShopDBEntities entities;

        /// <summary>
        /// Creates the repository.
        /// </summary>
        public ModelRepository(CarShopDBEntities entities)
        {
            this.entities = entities;
        }

        /// <inheritdoc />
        public IQueryable<Model> GetAll()
        {
            return entities.Models;
        }

        /// <inheritdoc />
        public void Save(Model model)
        {
            ThrowIfExists(model.Id, model.BaseModel, model.Extras.ToList());

            bool newModel = !entities.Models.Select(m => m.Id).Contains(model.Id);
            if (newModel)
            {
                model.Id = (entities.Models.Max(m => (int?)m.Id) ?? 0) + 1;
            }

            try
            {
                if (newModel)
                {
                    entities.Models.Add(model);
                }

                entities.SaveChanges();
            }
            catch // rollback if not successful, but exception will be thrown outside   
            {
                if (newModel)
                {
                    entities.Models.Remove(model);
                }

                entities.Entry(model).Reload();

                throw;
            }
        }

        /// <inheritdoc />
        public void Delete(Model model)
        {
            entities.Models.Remove(model);
            entities.SaveChanges();
        }

        /// <inheritdoc />
        public void ThrowIfExists(int id, BaseModel baseModel, IList<Extra> extras)
        {
            var modelExtraIds = extras.Select(e => e.Id);
            bool exists = entities.Models.Any(
                m =>
                    m.Id != id &&
                    m.BaseModel.Id == baseModel.Id &&
                    m.Extras.Count == extras.Count &&
                    m.Extras.Select(e => e.Id).Intersect(modelExtraIds).Count() == extras.Count);

            if (exists)
            {
                throw new InvalidOperationException("Nem lehet menteni - már létezik ilyen modell!");
            }
        }
    }
}

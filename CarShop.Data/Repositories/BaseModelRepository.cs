﻿using CarShop.Data.Interfaces;
using System.Linq;

namespace CarShop.Data.Repositories
{
    /// <summary>
    /// Repository of base models.
    /// </summary>
    public class BaseModelRepository : IRepository<BaseModel>
    {
        private CarShopDBEntities entities;

        /// <summary>
        /// Creates the repository.
        /// </summary>
        public BaseModelRepository(CarShopDBEntities entities)
        {
            this.entities = entities;
        }

        /// <inheritdoc />
        public IQueryable<BaseModel> GetAll()
        {
            return entities.BaseModels;
        }
    }
}

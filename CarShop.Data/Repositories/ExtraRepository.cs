﻿using CarShop.Data.Interfaces;
using System.Linq;

namespace CarShop.Data.Repositories
{
    /// <summary>
    /// Repository of extras 
    /// </summary>
    public class ExtraRepository : IRepository<Extra>
    {
        private CarShopDBEntities entities;

        /// <summary>
        /// Creates the repository.
        /// </summary>
        public ExtraRepository(CarShopDBEntities entities)
        {
            this.entities = entities;
        }

        /// <inheritdoc />
        public IQueryable<Extra> GetAll()
        {
            return entities.Extras;
        }
    }
}

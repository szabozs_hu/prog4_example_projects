﻿using System.Collections.Generic;

namespace CarShop.Data.Interfaces
{
    /// <summary>
    /// Repository of models
    /// </summary>
    public interface IModelRepository : IRepository<Model>
    {
        /// <summary>
        /// Save model. Will throw an exception if another model with the same settings 
        /// exists.
        /// </summary>
        void Save(Model model);

        /// <summary>
        /// Delete model.
        /// </summary>
        void Delete(Model model);

        /// <summary>
        /// Throws exception if another model with the same settings exists.
        /// </summary>
        void ThrowIfExists(int id, BaseModel baseModel, IList<Extra> extras);
    }
}

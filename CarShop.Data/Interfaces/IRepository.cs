﻿using System.Linq;

namespace CarShop.Data.Interfaces
{
    /// <summary>
    /// Repository of data objects.
    /// </summary>
    public interface IRepository<T>
    {
        /// <summary>
        /// Returns all data objects.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "A method is better here to emphasize complexity / prepare users for longer return time.")]
        IQueryable<T> GetAll();
    }
}

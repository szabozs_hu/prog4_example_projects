﻿using System.Windows;

namespace CarShop.GUI
{
    /// <summary>
    /// Interaction logic for BaseModelListWindow.xaml
    /// </summary>
    public partial class BaseModelListWindow : Window
    {
        public BaseModelListWindow()
        {
            InitializeComponent();
        }
    }
}

﻿using CarShop.Data;
using CarShop.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace CarShop.GUI
{
    /// <summary>
    /// ViewModel of ExtraListWindow
    /// </summary>
    internal class ExtraListWindowViewModel
    {
        private IRepository<Extra> repository;

        /// <summary>
        /// All extras 
        /// </summary>
        public IList<Extra> Extras
        {
            get { return repository.GetAll().ToList(); }
        }

        /// <summary>
        /// Creates the ViewModel.
        /// </summary>
        public ExtraListWindowViewModel(IRepository<Extra> repository)
        {
            this.repository = repository;
        }
    }
}

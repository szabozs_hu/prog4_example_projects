﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarShop.GUI.Services
{
    /// <summary>
    /// Opens web page.
    /// </summary>
    internal class WebpageService
    {
        /// <summary>
        /// Opens web page. 
        /// </summary>
        public void Open(string url)
        {
            if (url == null)
                return;

            using (Process process = new Process())
            {
                process.StartInfo.UseShellExecute = true;
                process.StartInfo.FileName = url;
                process.Start();
            }
        }
    }
}

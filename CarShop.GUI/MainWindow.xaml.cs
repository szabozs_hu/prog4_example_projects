﻿using CarShop.BL;
using CarShop.BL.Interfaces;
using CarShop.Data;
using CarShop.Data.Interfaces;
using CarShop.Data.Repositories;
using CarShop.GUI.Services;
using System;
using System.ComponentModel;
using System.Windows;

namespace CarShop.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable", Justification = "Closed event cleans up.")]
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private CarShopDBEntities entities;

        private IRepository<Brand> brandRepository;
        private IModelRepository modelRepository;
        private IRepository<Extra> extraRepository;
        private IRepository<BaseModel> baseModelRepository = null;

        private IModelPriceLogic modelPriceLogic;
        private IModelListLogic modelListLogic;
        private IBrandAndBaseModelSelectionLogic brandAndModelSelectionLogic;

        private MainWindowViewModel viewModel;

        private WebpageService webPageService;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            entities = new CarShopDBEntities();

            brandRepository = new BrandRepository(entities);
            modelRepository = new ModelRepository(entities);
            extraRepository = new ExtraRepository(entities);
            baseModelRepository = new BaseModelRepository(entities);

            modelPriceLogic = new ModelPriceLogic();
            modelListLogic = new ModelListLogic(modelRepository, modelPriceLogic);
            brandAndModelSelectionLogic = new BrandAndBaseModelSelectionLogic(brandRepository, baseModelRepository);

            viewModel = new MainWindowViewModel(modelListLogic, modelPriceLogic);
            this.DataContext = viewModel;

            webPageService = new WebpageService();
        }

        private void ModelList_Click(object sender, RoutedEventArgs e)
        {
            BaseModelListWindow modelList = new BaseModelListWindow();
            modelList.Owner = this;
            modelList.DataContext = new BaseModelListWindowViewModel(baseModelRepository);
            modelList.ShowDialog();
        }

        private void ExtraList_Click(object sender, RoutedEventArgs e)
        {
            ExtraListWindow extraList = new ExtraListWindow();
            extraList.Owner = this;
            extraList.DataContext = new ExtraListWindowViewModel(extraRepository);
            extraList.ShowDialog();
        }

        private void NewCar_Click(object sender, RoutedEventArgs e)
        {
            ShowCarDialog(null);
        }

        private void NewCarBasedOnSelection_Click(object sender, RoutedEventArgs e)
        {
            if (viewModel.SelectedModel != null)
            {
                ShowCarDialog(viewModel.SelectedModel.Model, true);
            }
        }

        private void ModifyCar_Click(object sender, RoutedEventArgs e)
        {
            if (viewModel.SelectedModel != null)
            {
                ShowCarDialog(viewModel.SelectedModel.Model);
            }
        }

        private void DeleteCar_Click(object sender, RoutedEventArgs e)
        {
            if (viewModel.SelectedModel != null)
            {
                viewModel.Delete(viewModel.SelectedModel.Model);
            }
        }

        private void TextBlockBrandName_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                webPageService.Open(viewModel.SelectedModel?.Model.BaseModel.Brand.URL);
            }
            catch (Win32Exception)
            {
                MessageBox.Show("Nem lehet megnyitni a weblapot.");
            }
        }

        private void ShowCarDialog(Model model, bool copy = false)
        {
            CarWindow carWindow = new CarWindow();
            carWindow.Owner = this;
            carWindow.DataContext = new CarWindowViewModel(
                extraRepository, 
                brandAndModelSelectionLogic, 
                modelListLogic, 
                modelPriceLogic, 
                model, 
                copy);

            carWindow.ShowDialog();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (entities != null)
            {
                entities.Dispose();
            }
        }
    }
}

﻿using CarShop.BL.Interfaces;
using CarShop.Data;

namespace CarShop.GUI
{
    /// <summary>
    /// Supports listing of Models together with their dynamically calculated prices.
    /// </summary>
    internal class PricedModel
    {
        private Model model;
        private IModelPriceLogic modelPriceLogic;

        /// <summary>
        /// Model
        /// </summary>
        public Model Model { get { return model; } }

        /// <summary>
        /// The price of the model
        /// </summary>
        public decimal Price { get { return modelPriceLogic.CalculatePrice(model); } }

        /// <summary>
        /// Creates the instance.
        /// </summary>
        public PricedModel(Model model, IModelPriceLogic modelPriceLogic)
        {
            this.model = model;
            this.modelPriceLogic = modelPriceLogic;       
        }
    }
}

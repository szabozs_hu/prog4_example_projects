﻿using CarShop.BL;
using CarShop.BL.Interfaces;
using CarShop.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarShop.GUI
{
    /// <summary>
    /// ViewModel for MainWindow
    /// </summary>
    internal class MainWindowViewModel : Bindable
    {
        private readonly IModelPriceLogic modelPriceLogic;
        private readonly IModelListLogic modelListLogic;

        private string brandFilter;
        private string baseModelFilter;
        private uint? minPriceFilter;
        private uint? maxPriceFilter;
        private PricedModel selectedModel;

        private IList<PricedModel> models;
        /// <summary>
        /// Models 
        /// </summary>
        public IList<PricedModel> Models
        {
            get
            {
                return models;
            }
        }

        /// <summary>
        /// Selected model 
        /// </summary>
        public PricedModel SelectedModel
        {
            get { return selectedModel; }
            set { selectedModel = value; OnPropertyChanged(nameof(SelectedModel)); }
        }

        /// <summary>
        /// Filter for brand 
        /// </summary>
        public string BrandFilter
        {
            get { return brandFilter; }
            set
            {
                brandFilter = value;
                OnPropertyChanged(nameof(BrandFilter));
                RefreshModels();
            }
        }

        /// <summary>
        /// Filter for model 
        /// </summary>
        public string BaseModelFilter
        {
            get { return baseModelFilter; }
            set
            {
                baseModelFilter = value;
                OnPropertyChanged(nameof(BaseModelFilter));
                RefreshModels();
            }
        }

        /// <summary>
        /// Filter for min price 
        /// </summary>
        public uint? MinPriceFilter
        {
            get { return minPriceFilter; }
            set
            {
                minPriceFilter = value;
                OnPropertyChanged(nameof(MinPriceFilter));
                RefreshModels();
            }
        }

        /// <summary>
        /// Filter for max price
        /// </summary>
        public uint? MaxPriceFilter
        {
            get { return maxPriceFilter; }
            set
            {
                maxPriceFilter = value;
                OnPropertyChanged(nameof(MaxPriceFilter));
                RefreshModels();
            }
        }

        /// <summary>
        /// ViewModel for MainWindow
        /// </summary>
        public MainWindowViewModel(IModelListLogic modelListLogic, IModelPriceLogic modelPriceLogic)
        {
            this.modelListLogic = modelListLogic;
            this.modelPriceLogic = modelPriceLogic;

            RefreshModels();

            modelListLogic.ModelListChanged += ModelListChanged;
        }

        /// <summary>
        /// Deletes model instance.
        /// </summary>
        public void Delete(Model model)
        {
            modelListLogic.Delete(model);
        }

        private void RefreshModels()
        {
            FilterSet filterSet = new FilterSet(brandFilter, baseModelFilter, minPriceFilter, maxPriceFilter);
            models = modelListLogic
                .GetFilteredModelList(filterSet)
                .Select(m => new PricedModel(m, modelPriceLogic)).ToList();

            OnPropertyChanged(nameof(Models));

            if (!models.Contains(SelectedModel))
            {
                SelectedModel = models.FirstOrDefault();
            }
        }

        private void ModelListChanged(object sender, EventArgs e)
        {
            RefreshModels();
        }
    }
}
﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace CarShop.GUI.Converters
{
    /// <summary>
    /// Converts string to nullable for min/max price bindings 
    /// </summary>
    internal class StringToNullableUintConverter : IValueConverter
    {
        /// <summary>
        /// Converts nullable number to string 
        /// </summary>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value?.ToString();
        }

        /// <summary>
        /// Converts string to nullable number 
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string stringValue = value as string;
            if (string.IsNullOrEmpty(stringValue))
            {
                return null;
            }

            return uint.Parse(stringValue);
        }
    }
}

﻿using CarShop.BL.Interfaces;
using CarShop.Data;
using CarShop.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CarShop.GUI
{
    /// <summary>
    /// ViewModel for CarWindow
    /// </summary>
    internal class CarWindowViewModel : Bindable
    {
        private Brand selectedBrand;
        private ObservableCollection<Brand> selectableBrands;
        private BaseModel selectedBaseModel;
        private ObservableCollection<BaseModel> selectableModels;
        private IList<Selectable<Extra>> selectableExtras;

        private IRepository<Extra> extraRepository;
        private IBrandAndBaseModelSelectionLogic brandAndModelSelectionLogic;
        private IModelPriceLogic modelPriceLogic;
        private IModelListLogic modelListLogic;

        private Model editedModel;

        /// <summary>
        /// Selected brand 
        /// </summary>
        public Brand SelectedBrand
        {
            get { return selectedBrand; }
            set
            {
                selectedBrand = value;
                OnPropertyChanged(nameof(SelectedBrand));
                RefreshSelectableModels();
            }
        }

        /// <summary>
        /// Selectable brands 
        /// </summary>
        public ObservableCollection<Brand> SelectableBrands
        {
            get { return selectableBrands; }
            private set
            {
                selectableBrands = value;
                OnPropertyChanged(nameof(SelectableBrands));
            }
        }

        /// <summary>
        /// Selected BaseModel
        /// </summary>
        public BaseModel SelectedModel
        {
            get { return selectedBaseModel; }
            set
            {
                selectedBaseModel = value;
                OnPropertyChanged(nameof(SelectedModel));
                OnPropertyChanged(nameof(FullPrice));
            }
        }

        /// <summary>
        /// Selectable BaseModels 
        /// </summary>
        public ObservableCollection<BaseModel> SelectableModels
        {
            get { return selectableModels; }
            private set
            {
                selectableModels = value;
                OnPropertyChanged(nameof(SelectableModels));
            }
        }

        /// <summary>
        /// Selectable extras 
        /// </summary>
        public IList<Selectable<Extra>> SelectableExtras
        {
            get { return selectableExtras; }
            set
            {
                selectableExtras = value;
                OnPropertyChanged(nameof(SelectableExtras));
            }
        }

        /// <summary>
        /// Full price calculated from all selections 
        /// </summary>
        public decimal FullPrice
        {
            get
            {
                if (SelectedModel != null)
                {
                    return modelPriceLogic.CalculatePrice(
                        SelectedModel,
                        SelectableExtras.Where(ex => ex.IsSelected).Select(ex => ex.SelectableItem));
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Brand change is enabled 
        /// </summary>
        public bool EnableBrandChange { get; } = true;

        /// <summary>
        /// Model change is enabled
        /// </summary>
        public bool EnableBaseModelChange { get; } = true;

        private CarWindowViewModel(
            IRepository<Extra> extraRepository,
            IBrandAndBaseModelSelectionLogic brandAndModelSelectionLogic,
            IModelListLogic modelListLogic,
            IModelPriceLogic modelPriceLogic)
        {
            this.extraRepository = extraRepository;
            this.brandAndModelSelectionLogic = brandAndModelSelectionLogic;
            this.modelPriceLogic = modelPriceLogic;
            this.modelListLogic = modelListLogic;
        }

        /// <summary>
        /// Creates the viewmodel
        /// </summary>
        public CarWindowViewModel(
            IRepository<Extra> extraRepository,
            IBrandAndBaseModelSelectionLogic brandAndModelSelectionLogic,
            IModelListLogic modelListLogic,
            IModelPriceLogic modelPriceLogic,
            Model model,
            bool copyModelSettings)
            : this(extraRepository, brandAndModelSelectionLogic, modelListLogic, modelPriceLogic)
        {
            if (copyModelSettings && model == null)
            {
                throw new ArgumentException("CopyModelSettings = true is invalid without a model.", nameof(copyModelSettings));
            }

            this.editedModel = (model != null && !copyModelSettings) ? model : null;

            var modelToSelect = model?.BaseModel;
            var brandToSelect = model?.BaseModel.Brand;
            SelectableBrands = new ObservableCollection<Brand>(brandAndModelSelectionLogic.GetSelectableBrands(modelToSelect));
            SelectableModels = new ObservableCollection<BaseModel>(brandAndModelSelectionLogic.GetSelectableBaseModels(brandToSelect));
            CreateSelectableExtraList(model?.Extras?.ToList());

            SelectedModel = modelToSelect;
            SelectedBrand = brandToSelect ?? SelectableBrands.FirstOrDefault();

            EnableBrandChange = model == null;
            EnableBaseModelChange = model == null;
        }

        /// <summary>
        /// Saves a model with the current selections. If an existing model is edited, 
        /// it will save the existing model instance. 
        /// </summary>
        public void Save()
        {
            var savedModel = editedModel ?? new Model();

            // this is to avoid saving the new basemodel/extras into the model if a model like this already exists  
            modelListLogic.ThrowIfExists(
                savedModel.Id,
                SelectedModel,
                SelectableExtras.Where(ex => ex.IsSelected).Select(ex => ex.SelectableItem).ToList());

            savedModel.BaseModel = SelectedModel;
            savedModel.Extras = SelectableExtras.Where(ex => ex.IsSelected).Select(ex => ex.SelectableItem).ToList();

            modelListLogic.Save(savedModel);

            editedModel = savedModel;
        }

        /// <summary>
        /// Refreshes list of selectable models based on the brand that is selected
        /// </summary>
        private void RefreshSelectableModels()
        {
            SelectableModels = new ObservableCollection<BaseModel>(brandAndModelSelectionLogic.GetSelectableBaseModels(SelectedBrand));

            if (!SelectableModels.Contains(SelectedModel))
            {
                SelectedModel = SelectableModels.FirstOrDefault();
            }
        }

        /// <summary>
        /// Creates list of selectable extras and subscribes their changes 
        /// </summary>
        private void CreateSelectableExtraList(IEnumerable<Extra> extrasToSelect)
        {
            if (SelectableExtras != null)
            {
                UnsubscribeExtraSelectionChanges();
            }

            SelectableExtras = 
                extraRepository.GetAll()
                .ToList()
                .Select(extra => new Selectable<Extra>(extra, extrasToSelect?.Contains(extra) == true)).ToList();

            SubscribeExtraSelectionChanges();
        }

        /// <summary>
        /// Subscribes to the changes of the selectable extras 
        /// </summary>
        private void SubscribeExtraSelectionChanges()
        {
            foreach (var selectable in SelectableExtras)
            {
                selectable.PropertyChanged -= SelectablePropertyChanged; // extra security to avoid accidental duplicate subscriptions,
                                                                         // not really necessary :) 
                selectable.PropertyChanged += SelectablePropertyChanged;
            }
        }

        /// <summary>
        /// Unsubscribes from the changes of the selectable extras 
        /// </summary>
        private void UnsubscribeExtraSelectionChanges()
        {
            foreach (var selectable in SelectableExtras)
            {
                selectable.PropertyChanged -= SelectablePropertyChanged;
            }
        }

        /// <summary>
        /// Fires PropertyChanged when a selectable is selected 
        /// </summary>
        private void SelectablePropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(Selectable<Extra>.IsSelected))
            {
                OnPropertyChanged(nameof(FullPrice));
            }
        }
    }
}

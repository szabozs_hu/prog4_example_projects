﻿using System.ComponentModel;

namespace CarShop.GUI
{
    /// <summary>
    /// Base of bindable objects 
    /// </summary>
    internal abstract class Bindable : INotifyPropertyChanged
    { 
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <inheritdoc />
        public event PropertyChangedEventHandler PropertyChanged;
    }
}

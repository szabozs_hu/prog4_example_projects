﻿using System;
using System.Windows;

namespace CarShop.GUI
{
    /// <summary>
    /// Interaction logic for CarWindow.xaml
    /// </summary>
    public partial class CarWindow : Window
    {
        public CarWindow()
        {
            InitializeComponent();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = (CarWindowViewModel)DataContext;
            try
            {
                viewModel.Save();
                MessageBox.Show("Sikeres mentés.");
                this.DialogResult = true;
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

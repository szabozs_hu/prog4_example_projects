﻿namespace CarShop.GUI
{
    /// <summary>
    /// Supports a fixed list of objects that are selectable in a ListBox/ItemsControl
    /// via checkboxes. 
    /// </summary>
    internal class Selectable<T> : Bindable
    {
        private bool isSelected;

        /// <summary>
        /// Item that is selected (or not).
        /// </summary>
        public T SelectableItem { get; }

        /// <summary>
        /// Whether it is selected (or not).
        /// </summary>
        public bool IsSelected
        {
            get { return isSelected; }
            set { isSelected = value; OnPropertyChanged(nameof(IsSelected)); }
        }

        /// <summary>
        /// Creates the instance.
        /// </summary>
        public Selectable(T selectableItem, bool isSelected = false)
        {
            SelectableItem = selectableItem;
            IsSelected = isSelected;
        }
    }
}

﻿using CarShop.BL;
using CarShop.BL.Interfaces;
using CarShop.Data;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace CarShop.GUI.Tests.Pro_MockedTests
{
    /// <summary>
    /// Tests for MainWindowViewModel
    /// </summary>
    [TestFixture]
    internal class MainWindowViewModelTests_WithMocks
    {
        private Mock<IModelListLogic> modelListLogicMock;
        private Mock<IModelPriceLogic> modelPriceLogicMock;
        private MainWindowViewModel viewModel;

        /// <summary>
        /// Runs before every test 
        /// </summary>
        [SetUp]
        public void Setup()
        {
            modelListLogicMock = new Mock<IModelListLogic>();
            modelListLogicMock.Setup(logic => logic.GetFilteredModelList(It.IsAny<FilterSet>())).Returns(new Model[0]);

            modelPriceLogicMock = new Mock<IModelPriceLogic>();
            viewModel = new MainWindowViewModel(modelListLogicMock.Object, modelPriceLogicMock.Object);
        }

        /// <summary>
        /// Check that when the Models property is called, it calls into the ModelListLogic with 
        /// the filters that were previously set. 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "models", Justification = "The test is about the get method of this property.")]
        [Test]
        public void WhenSettingFilterRelevantProperty_ReturnsFilteredModelList()
        {
            // ACT 
            viewModel.BrandFilter = "Opel";
            viewModel.MaxPriceFilter = 500;
            viewModel.MinPriceFilter = 100;
            viewModel.BaseModelFilter = "Combo";

            // ASSERT 
            modelListLogicMock.Verify(logic => logic.GetFilteredModelList(
                It.Is<FilterSet>(filterSet =>
                    filterSet.BrandFilter == "Opel" &&
                    filterSet.MaxPriceFilter == uint.MaxValue &&
                    filterSet.MinPriceFilter == uint.MinValue &&
                    filterSet.BaseModelFilter == null)), Times.Once);

            modelListLogicMock.Verify(logic => logic.GetFilteredModelList(
                It.Is<FilterSet>(filterSet =>
                    filterSet.BrandFilter == "Opel" &&
                    filterSet.MaxPriceFilter == 500 &&
                    filterSet.MinPriceFilter == uint.MinValue &&
                    filterSet.BaseModelFilter == null)), Times.Once);

            modelListLogicMock.Verify(logic => logic.GetFilteredModelList(
                It.Is<FilterSet>(filterSet =>
                    filterSet.BrandFilter == "Opel" &&
                    filterSet.MaxPriceFilter == 500 &&
                    filterSet.MinPriceFilter == 100 &&
                    filterSet.BaseModelFilter == null)), Times.Once);

            modelListLogicMock.Verify(logic => logic.GetFilteredModelList(
                It.Is<FilterSet>(filterSet =>
                    filterSet.BrandFilter == "Opel" &&
                    filterSet.MaxPriceFilter == 500 &&
                    filterSet.MinPriceFilter == 100 &&
                    filterSet.BaseModelFilter == "Combo")), Times.Once);
        }

        /// <summary>
        ///  Check that all properties that are relevant to filtering fire propertychanged 
        ///  for themselves and the Model list too 
        /// </summary>
        [TestCase(nameof(MainWindowViewModel.BrandFilter), "irrelevant")]
        [TestCase(nameof(MainWindowViewModel.MaxPriceFilter), (uint)500)]
        [TestCase(nameof(MainWindowViewModel.MinPriceFilter), (uint)100)]
        [TestCase(nameof(MainWindowViewModel.BaseModelFilter), "irrelevant")]
        public void WhenSettingFilterRelevantProperty_PropertyAndModelsPropertyChanged(
            string filterRelevantProperty, object valueToSet)
        {
            // ARRANGE 
            Dictionary<string, object> propertyChangedCalls = new Dictionary<string, object>();
            viewModel.PropertyChanged += (s, e) => propertyChangedCalls[e.PropertyName] = s;

            // ACT 
            // setting property with reflection to be able to use string property names in testcases
            viewModel.GetType().GetProperty(filterRelevantProperty).SetValue(viewModel, valueToSet);

            // ASSERT 
            // at least 2 propertychanged events... (3 might be present if the selection has also changed)   
            Assert.That(propertyChangedCalls.Keys.Count, Is.InRange(2, 3));
            if (propertyChangedCalls.Keys.Count == 3)
            {
                Assert.That(propertyChangedCalls.ContainsKey(nameof(MainWindowViewModel.SelectedModel)));
            }
            // ... one for the current property ... 
            Assert.That(propertyChangedCalls.ContainsKey(filterRelevantProperty));
            // ... one for the Models property ... 
            Assert.That(propertyChangedCalls.ContainsKey(nameof(MainWindowViewModel.Models)));
            // ... and sender was always 'viewModel'
            Assert.That(propertyChangedCalls.Values.All(v => v == viewModel));
        }

        /// <summary>
        /// Selected model fires PropertyChanged 
        /// </summary>
        public void WhenSettingSelectedModel_SelectedModelPropertyChanged()
        {
            // ARRANGE 
            Dictionary<string, object> propertyChangedCalls = new Dictionary<string, object>();
            viewModel.PropertyChanged += (s, e) => propertyChangedCalls[e.PropertyName] = s;

            // ACT 
            viewModel.BrandFilter = "irrelevant";

            // ASSERT 
            Assert.That(propertyChangedCalls.Keys.Count, Is.EqualTo(1));
            Assert.That(propertyChangedCalls.ContainsKey(nameof(MainWindowViewModel.SelectedModel)));
            Assert.That(propertyChangedCalls[nameof(MainWindowViewModel.SelectedModel)], Is.EqualTo(viewModel));
        }
    }
}

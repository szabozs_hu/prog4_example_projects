﻿using CarShop.Data;
using CarShop.Data.Interfaces;
using Moq;
using NUnit.Framework;
using System.Linq;

namespace CarShop.GUI.Tests.Pro_MockedTests
{
    /// <summary>
    /// Tests for ModelListWindowViewModel
    /// </summary>
    [TestFixture]
    internal class ModelListWindowViewModelTests_WithMocks
    {
        [Test]
        public void WhenGettingModels_ReturnsModelListFromRepository()
        {
            // ARRANGE 
            var expectedModels = new[] { new BaseModel(), new BaseModel() };

            var modelRepositoryMock = new Mock<IRepository<BaseModel>>();
            modelRepositoryMock.Setup(r => r.GetAll()).Returns(expectedModels.AsQueryable());

            var viewModel = new BaseModelListWindowViewModel(modelRepositoryMock.Object);

            // ACT 
            var models = viewModel.BaseModels;

            // ASSERT 
            Assert.That(models, Is.EquivalentTo(expectedModels));
        }
    }
}

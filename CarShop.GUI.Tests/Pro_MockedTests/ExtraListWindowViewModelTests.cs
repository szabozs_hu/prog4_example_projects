﻿using CarShop.Data;
using CarShop.Data.Interfaces;
using Moq;
using NUnit.Framework;
using System.Linq;

namespace CarShop.GUI.Tests.Pro_MockedTests
{
    /// <summary>
    /// Tests for ExtraListWindowViewModel
    /// </summary>
    [TestFixture]
    internal class ExtraListWindowViewModelTests_WithMocks
    {
        [Test]
        public void WhenGettingExtras_ReturnsExtraListFromRepository()
        {
            // ARRANGE 
            var expectedExtras = new[] { new Extra(), new Extra() };

            var extraRepositoryMock = new Mock<IRepository<Extra>>();
            extraRepositoryMock.Setup(r => r.GetAll()).Returns(expectedExtras.AsQueryable());

            var viewModel = new ExtraListWindowViewModel(extraRepositoryMock.Object);

            // ACT 
            var extras = viewModel.Extras;

            // ASSERT 
            Assert.That(extras, Is.EquivalentTo(expectedExtras));
        }
    }
}

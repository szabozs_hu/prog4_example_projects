﻿using CarShop.BL.Interfaces;
using CarShop.Data;
using CarShop.Data.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarShop.GUI.Tests.Pro_MockedTests
{
    /// <summary>
    /// Tests for CarWindowViewModel
    /// </summary>
    [TestFixture]
    internal class CarWindowViewModelTests_WithMocks
    {
        private Mock<IRepository<Extra>> extraRepository;
        private Mock<IBrandAndBaseModelSelectionLogic> brandAndModelSelectionLogic;
        private Mock<IModelPriceLogic> modelPriceLogic;
        private Mock<IModelListLogic> modelListLogic;

        [SetUp]
        public void Setup()
        {
            extraRepository = new Mock<IRepository<Extra>>();
            brandAndModelSelectionLogic = new Mock<IBrandAndBaseModelSelectionLogic>();
            modelPriceLogic = new Mock<IModelPriceLogic>();
            modelListLogic = new Mock<IModelListLogic>();
        }
        
        [Test]
        public void WhenSettingSelectedBrand_SelectableModelsAreFilteredViaSelectionLogic_AndSelectedModelIsUpdated()
        {
            // ARRANGE 
            var brands = MakeEmptyObjects<Brand>(2);
            var models = MakeEmptyObjects<BaseModel>(5);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBaseModels(It.IsAny<Brand>())).Returns(models);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBrands(It.IsAny<BaseModel>())).Returns(brands);

            var viewModel = new CarWindowViewModel(extraRepository.Object, brandAndModelSelectionLogic.Object, modelListLogic.Object, modelPriceLogic.Object, null, false);
            Dictionary<string, object> propertyChangedCalls = new Dictionary<string, object>();
            viewModel.PropertyChanged += (s, e) => propertyChangedCalls[e.PropertyName] = s;

            brandAndModelSelectionLogic.ResetCalls();

            // ACT 
            viewModel.SelectedBrand = brands[0];

            // ASSERT 
            // filter logic is called 
            brandAndModelSelectionLogic.Verify(b => b.GetSelectableBaseModels(brands[0]), Times.Once);
            // first model of returned selectables is selected 
            Assert.That(viewModel.SelectedModel, Is.EqualTo(models[0]));
            // multiple events will be thrown, only checking for the SelectedBrand. Others are checked in their own tests.
            Assert.That(propertyChangedCalls.ContainsKey(nameof(CarWindowViewModel.SelectedBrand)));
            Assert.That(propertyChangedCalls.Values.All(v => v == viewModel));
        }

        [Test]
        public void WhenSettingSelectedBrand_PropertyChangedEventIsFired()
        {
            // ARRANGE 
            var brands = MakeEmptyObjects<Brand>(2);
            var models = MakeEmptyObjects<BaseModel>(5);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBaseModels(It.IsAny<Brand>())).Returns(models);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBrands(It.IsAny<BaseModel>())).Returns(brands);

            var viewModel = new CarWindowViewModel(extraRepository.Object, brandAndModelSelectionLogic.Object, modelListLogic.Object, modelPriceLogic.Object, null, false);
            Dictionary<string, object> propertyChangedCalls = new Dictionary<string, object>();
            viewModel.PropertyChanged += (s, e) => propertyChangedCalls[e.PropertyName] = s;

            // ACT 
            viewModel.SelectedBrand = brands[1];

            // ASSERT 
            // multiple events will be thrown, only checking for the SelectedBrand. 
            Assert.That(propertyChangedCalls.ContainsKey(nameof(CarWindowViewModel.SelectedBrand)));
            Assert.That(propertyChangedCalls.Values.All(v => v == viewModel));
        }
        
        [Test]
        public void WhenSettingExtras_PropertyChangedEventIsFired()
        {
            // ARRANGE 
            var brands = MakeEmptyObjects<Brand>(2);
            var models = MakeEmptyObjects<BaseModel>(5);
            var extras = MakeEmptyObjects<Extra>(5);
            extraRepository.Setup(m => m.GetAll()).Returns(extras.AsQueryable());
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBaseModels(It.IsAny<Brand>())).Returns(models);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBrands(It.IsAny<BaseModel>())).Returns(brands);

            var viewModel = new CarWindowViewModel(extraRepository.Object, brandAndModelSelectionLogic.Object, modelListLogic.Object, modelPriceLogic.Object, null, false);
            int calls = 0;
            viewModel.PropertyChanged += (s, e) =>
            {
                Assert.That(s, Is.EqualTo(viewModel));
                Assert.That(e.PropertyName, Is.EqualTo(nameof(CarWindowViewModel.FullPrice)));
                calls++;
            };

            // ACT 
            viewModel.SelectableExtras[1].IsSelected = true;
            viewModel.SelectableExtras[3].IsSelected = true;

            // ASSERT 
            Assert.That(calls, Is.EqualTo(2));
        }

        [Test]
        public void WhenSettingSelectedModel_PropertyChangedEventIsFired()
        {
            // ARRANGE 
            var brands = MakeEmptyObjects<Brand>(2);
            var models = MakeEmptyObjects<BaseModel>(5);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBaseModels(It.IsAny<Brand>())).Returns(models);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBrands(It.IsAny<BaseModel>())).Returns(brands);

            var viewModel = new CarWindowViewModel(extraRepository.Object, brandAndModelSelectionLogic.Object, modelListLogic.Object, modelPriceLogic.Object, null, false);
            Dictionary<string, object> propertyChangedCalls = new Dictionary<string, object>();
            viewModel.PropertyChanged += (s, e) => propertyChangedCalls[e.PropertyName] = s;

            // ACT 
            viewModel.SelectedModel = models[1];

            // ASSERT 
            Assert.That(propertyChangedCalls.Count, Is.EqualTo(2));
            Assert.That(propertyChangedCalls.ContainsKey(nameof(CarWindowViewModel.SelectedModel)));
            Assert.That(propertyChangedCalls.ContainsKey(nameof(CarWindowViewModel.FullPrice)));
            Assert.That(propertyChangedCalls.Values.All(v => v == viewModel));
        }

        [Test]
        public void WhenGettingFullPrice_PriceIsCalculatedBasedOnTheCurrentSelections()
        {
            // ARRANGE 
            var brands = MakeEmptyObjects<Brand>(2);
            var models = MakeEmptyObjects<BaseModel>(5);
            var extras = MakeEmptyObjects<Extra>(5);
            extraRepository.Setup(m => m.GetAll()).Returns(extras.AsQueryable());
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBaseModels(It.IsAny<Brand>())).Returns(models);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBrands(It.IsAny<BaseModel>())).Returns(brands);

            models[3].Price = 1111;
            models[3].Brand = brands[0];
            extras[2].Price = 333;
            extras[4].Price = 444;
            var expectedExtras = new[] { extras[2], extras[4] };

            modelPriceLogic
                .Setup(m => m.CalculatePrice(models[3], It.Is<IEnumerable<Extra>>(xtras => xtras.SequenceEqual(expectedExtras))))
                .Returns(1111 + 333 + 444);
            
            var viewModel = new CarWindowViewModel(extraRepository.Object, brandAndModelSelectionLogic.Object, modelListLogic.Object, modelPriceLogic.Object, null, false);

            // ACT 
            viewModel.SelectedBrand = models[3].Brand;
            viewModel.SelectedModel = models[3];
            viewModel.SelectableExtras[2].IsSelected = true;
            viewModel.SelectableExtras[4].IsSelected = true;

            // ASSERT 
            Assert.That(viewModel.FullPrice, Is.EqualTo(1111 + 333 + 444));
        }

        [Test]
        public void WhenCreatingWindow_BrandAndModelListIsInitializedViaLogic()
        {
            // ARRANGE 
            var brands = MakeEmptyObjects<Brand>(2);
            var models = MakeEmptyObjects<BaseModel>(5);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBaseModels(It.IsAny<Brand>())).Returns(models);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBrands(It.IsAny<BaseModel>())).Returns(brands);

            // ACT 
            var viewModel = new CarWindowViewModel(extraRepository.Object, brandAndModelSelectionLogic.Object, modelListLogic.Object, modelPriceLogic.Object, null, false);

            // ASSERT 
            Assert.That(viewModel.SelectableBrands, Is.EquivalentTo(brands));
            Assert.That(viewModel.SelectableModels, Is.EquivalentTo(models));
            Assert.That(viewModel.SelectedBrand, Is.EqualTo(brands[0]));
            Assert.That(viewModel.SelectedModel, Is.EqualTo(models[0]));
        }

        [Test]
        public void WhenCreatingNewCar_AllExtrasAreUnselected()
        {
            // ARRANGE 
            var brands = MakeEmptyObjects<Brand>(2);
            var models = MakeEmptyObjects<BaseModel>(5);
            var extras = MakeEmptyObjects<Extra>(5);
            extraRepository.Setup(m => m.GetAll()).Returns(extras.AsQueryable());
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBaseModels(It.IsAny<Brand>())).Returns(models);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBrands(It.IsAny<BaseModel>())).Returns(brands);

            // ACT 
            var viewModel = new CarWindowViewModel(extraRepository.Object, brandAndModelSelectionLogic.Object, modelListLogic.Object, modelPriceLogic.Object, null, false);

            // ASSERT 
            Assert.That(viewModel.SelectableExtras.Select(se => se.SelectableItem), Is.EquivalentTo(extras));
            Assert.That(viewModel.SelectableExtras.All(se => se.IsSelected == false));
        }

        [Test]
        public void WhenCreatingNewCar_BrandAndModelChangeIsAllowed()
        {
            var brands = MakeEmptyObjects<Brand>(2);
            var models = MakeEmptyObjects<BaseModel>(5);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBaseModels(It.IsAny<Brand>())).Returns(models);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBrands(It.IsAny<BaseModel>())).Returns(brands);

            // ACT 
            var viewModel = new CarWindowViewModel(extraRepository.Object, brandAndModelSelectionLogic.Object, modelListLogic.Object, modelPriceLogic.Object, null, false);

            // ASSERT 
            Assert.That(viewModel.EnableBrandChange, Is.True);
            Assert.That(viewModel.EnableBaseModelChange, Is.True);
        }

        [TestCase(false)]
        [TestCase(true)]
        public void WhenModifyingOrCopyingCar_BrandAndModelIsSelected(bool copy)
        {
            // ARRANGE 
            var brands = MakeEmptyObjects<Brand>(2);
            var baseModels = MakeEmptyObjects<BaseModel>(5);
            var extras = MakeEmptyObjects<Extra>(5);

            baseModels[3].Brand = brands[1];
            var model = new Model() { BaseModel = baseModels[3] };

            extraRepository.Setup(m => m.GetAll()).Returns(extras.AsQueryable());
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBaseModels(model.BaseModel.Brand)).Returns(baseModels);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBrands(model.BaseModel)).Returns(new[] { model.BaseModel.Brand });

            // ACT 
            var viewModel = new CarWindowViewModel(extraRepository.Object, brandAndModelSelectionLogic.Object, modelListLogic.Object, modelPriceLogic.Object, model, copy);

            // ASSERT 
            Assert.That(viewModel.SelectedBrand, Is.EqualTo(model.BaseModel.Brand));
            Assert.That(viewModel.SelectedModel, Is.EqualTo(model.BaseModel));
        }

        [TestCase(false)]
        [TestCase(true)]
        public void WhenModifyingOrCopyingCar_BrandAndModelChangeIsNotAllowed(bool copy)
        {
            // ARRANGE 
            var brands = MakeEmptyObjects<Brand>(1);
            var models = MakeEmptyObjects<BaseModel>(1);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBaseModels(It.IsAny<Brand>())).Returns(models);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBrands(It.IsAny<BaseModel>())).Returns(brands);
            models[0].Brand = brands[0];
            var model = new Model() { BaseModel = models[0] };

            // ACT 
            var viewModel = new CarWindowViewModel(extraRepository.Object, brandAndModelSelectionLogic.Object, modelListLogic.Object, modelPriceLogic.Object, model, copy);

            // ASSERT 
            Assert.That(viewModel.EnableBrandChange, Is.False);
            Assert.That(viewModel.EnableBaseModelChange, Is.False);
        }

        [TestCase(false)]
        [TestCase(true)]
        public void WhenModifyingOrCopyingCar_ItsExtrasAreSelected(bool copy)
        {
            // ARRANGE 
            var brands = MakeEmptyObjects<Brand>(2);
            var baseModels = MakeEmptyObjects<BaseModel>(5);
            var extras = MakeEmptyObjects<Extra>(5);

            var expectedExtras = extras.Skip(1).Take(3).ToList();
            baseModels[3].Brand = brands[1];
            var model = new Model() { BaseModel = baseModels[3], Extras = expectedExtras };

            extraRepository.Setup(m => m.GetAll()).Returns(extras.AsQueryable());
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBaseModels(model.BaseModel.Brand)).Returns(baseModels);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBrands(model.BaseModel)).Returns(new[] { model.BaseModel.Brand });

            // ACT 
            var viewModel = new CarWindowViewModel(extraRepository.Object, brandAndModelSelectionLogic.Object, modelListLogic.Object, modelPriceLogic.Object, model, copy);

            // ASSERT 
            var selectedExtras = viewModel.SelectableExtras.Where(se => se.IsSelected);
            Assert.That(selectedExtras.Where(se => se.IsSelected).Select(se => se.SelectableItem), 
                Is.EquivalentTo(expectedExtras));
        }

        public static IEnumerable<object[]> EditedModel
        {
            get
            {
                yield return new object[] { null };
                yield return new object[] { new Model() { Id = 7, BaseModel = new BaseModel() } };
            }
        }

        [TestCaseSource(nameof(EditedModel))]
        public void WhenCreatingOrCopyingCar_SaveIsCalledWithNewModel(Model editedModel)
        {
            // ARRANGE 
            var brands = MakeEmptyObjects<Brand>(2);
            var models = editedModel == null ? MakeEmptyObjects<BaseModel>(1) : new[] { editedModel.BaseModel };
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBaseModels(It.IsAny<Brand>())).Returns(models);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBrands(It.IsAny<BaseModel>())).Returns(brands);

            modelListLogic.Setup(m => m.Save(It.IsAny<Model>()));

            bool copy = editedModel != null;
            var viewModel = new CarWindowViewModel(extraRepository.Object, brandAndModelSelectionLogic.Object, modelListLogic.Object, modelPriceLogic.Object, editedModel, copy);

            // ACT 
            viewModel.Save();

            // ASSERT 
            modelListLogic.Verify(m => m.Save(It.IsAny<Model>()), Times.Once);
            modelListLogic.Verify(m => m.Save(It.Is<Model>(o => o.Id == 0)));
        }

        [Test]
        public void WhenModifyingCar_SaveIsCalledWithModifiedModel()
        {
            // ARRANGE 
            var brands = MakeEmptyObjects<Brand>(2);
            var models = MakeEmptyObjects<BaseModel>(5);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBaseModels(It.IsAny<Brand>())).Returns(models);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBrands(It.IsAny<BaseModel>())).Returns(brands);

            modelListLogic.Setup(m => m.Save(It.IsAny<Model>()));

            var model = new Model() { Id = 8, BaseModel = models[0] };

            var viewModel = new CarWindowViewModel(extraRepository.Object, brandAndModelSelectionLogic.Object, modelListLogic.Object, modelPriceLogic.Object, model, false);

            // ACT 
            viewModel.Save();

            // ASSERT 
            modelListLogic.Verify(m => m.Save(It.IsAny<Model>()), Times.Once);
            modelListLogic.Verify(m => m.Save(model));
        }

        [Test]
        public void WhenSaveIsCalledAndSimilarModelExists_ThrowsException()
        {
            // ARRANGE 
            var brands = MakeEmptyObjects<Brand>(2);
            var models = MakeEmptyObjects<BaseModel>(5);
            var extras = MakeEmptyObjects<Extra>(5);
            extraRepository.Setup(m => m.GetAll()).Returns(extras.AsQueryable());
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBaseModels(It.IsAny<Brand>())).Returns(models);
            brandAndModelSelectionLogic.Setup(m => m.GetSelectableBrands(It.IsAny<BaseModel>())).Returns(brands);

            modelListLogic
                .Setup(m => m.ThrowIfExists(8, models[0], It.Is<IList<Extra>>(l => l.SequenceEqual(extras))))
                .Callback(() => throw new Exception());

            var model = new Model() { Id = 8, BaseModel = models[0], Extras = extras };

            var viewModel = new CarWindowViewModel(extraRepository.Object, brandAndModelSelectionLogic.Object, modelListLogic.Object, modelPriceLogic.Object, model, false);

            // ACT 
            // ASSERT 
            Assert.Throws(typeof(Exception), () => viewModel.Save());
        }
        
        private static T[] MakeEmptyObjects<T>(int count)
            where T: new()
        {
            T[] dataObjects = new T[count];
            for (int i = 0; i < count; i++)
            {
                dataObjects[i] = new T();
            }

            return dataObjects;
        }
    }
}

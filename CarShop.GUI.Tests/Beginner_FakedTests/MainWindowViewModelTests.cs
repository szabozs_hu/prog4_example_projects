﻿using CarShop.GUI.Tests.Beginner_FakedTests.Fakes;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace CarShop.GUI.Tests.Beginner_FakedTests
{
    /// <summary>
    /// Tests for MainWindowViewModel
    /// </summary>
    [TestFixture]
    internal class MainWindowViewModelTests_WithFakes
    {
        private FakeModelListLogic logic;
        private FakeModelPriceLogic modelPriceLogic;
        private MainWindowViewModel viewModel;

        /// <summary>
        /// Runs before every test 
        /// </summary>
        [SetUp]
        public void Setup()
        {
            logic = new FakeModelListLogic();
            modelPriceLogic = new FakeModelPriceLogic();
            viewModel = new MainWindowViewModel(logic, modelPriceLogic);
        }

        /// <summary>
        /// Check that when the Models property is called, it calls into the ModelListLogic with 
        /// the filters that were previously set. 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "models", Justification = "The test is about the get method of this property.")]
        [Test]
        public void WhenGettingModels_ReturnsFilteredModelList()
        {
            // ARRANGE
            viewModel.BrandFilter = "Opel";
            viewModel.MaxPriceFilter = 500;
            viewModel.MinPriceFilter = 100;
            viewModel.BaseModelFilter = "Combo";

            // ACT 
            var models = viewModel.Models;

            // ASSERT 
            Assert.That(logic.FilterSetOfLastCall.BrandFilter, Is.EqualTo("Opel"));
            Assert.That(logic.FilterSetOfLastCall.MaxPriceFilter, Is.EqualTo(500));
            Assert.That(logic.FilterSetOfLastCall.MinPriceFilter, Is.EqualTo(100));
            Assert.That(logic.FilterSetOfLastCall.BaseModelFilter, Is.EqualTo("Combo"));
        }

        /// <summary>
        ///  Check that all properties that are relevant to filtering fire propertychanged 
        ///  for themselves and the Model list too 
        /// </summary>
        [TestCase(nameof(MainWindowViewModel.BrandFilter), "irrelevant")]
        [TestCase(nameof(MainWindowViewModel.MaxPriceFilter), (uint)500)]
        [TestCase(nameof(MainWindowViewModel.MinPriceFilter), (uint)100)]
        [TestCase(nameof(MainWindowViewModel.BaseModelFilter), "irrelevant")]
        public void WhenSettingFilterRelevantProperty_PropertyAndModelsPropertyChanged(
            string filterRelevantProperty, object valueToSet)
        {
            // ARRANGE 
            Dictionary<string, object> propertyChangedCalls = new Dictionary<string, object>();
            viewModel.PropertyChanged += (s, e) => propertyChangedCalls[e.PropertyName] = s;

            // ACT 
            // setting property with reflection to be able to use string property names in testcases
            viewModel.GetType().GetProperty(filterRelevantProperty).SetValue(viewModel, valueToSet);

            // ASSERT 
            // at least 2 propertychanged events... (3 might be present if the selection has also changed)   
            Assert.That(propertyChangedCalls.Keys.Count, Is.InRange(2, 3));
            if (propertyChangedCalls.Keys.Count == 3)
            {
                Assert.That(propertyChangedCalls.ContainsKey(nameof(MainWindowViewModel.SelectedModel)));
            }
            // ... one for the current property ... 
            Assert.That(propertyChangedCalls.ContainsKey(filterRelevantProperty));
            // ... one for the Models property ... 
            Assert.That(propertyChangedCalls.ContainsKey(nameof(MainWindowViewModel.Models)));
            // ... and sender was always 'viewModel'
            Assert.That(propertyChangedCalls.Values.All(v => v == viewModel));
        }

        /// <summary>
        /// Selected model fires PropertyChanged 
        /// </summary>
        public void WhenSettingSelectedModel_SelectedModelPropertyChanged()
        {
            // ARRANGE 
            Dictionary<string, object> propertyChangedCalls = new Dictionary<string, object>();
            viewModel.PropertyChanged += (s, e) => propertyChangedCalls[e.PropertyName] = s;

            // ACT 
            viewModel.BrandFilter = "irrelevant";

            // ASSERT 
            Assert.That(propertyChangedCalls.Keys.Count, Is.EqualTo(1));
            Assert.That(propertyChangedCalls.ContainsKey(nameof(MainWindowViewModel.SelectedModel)));
            Assert.That(propertyChangedCalls[nameof(MainWindowViewModel.SelectedModel)], Is.EqualTo(viewModel));
        }
    }
}

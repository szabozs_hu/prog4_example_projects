﻿using CarShop.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace CarShop.GUI.Tests.Beginner_FakedTests.Fakes
{
    /// <summary>
    /// This is a repository implementor without any meaningful logic, to be used in tests. 
    /// </summary>
    internal class FakeRepository<T> : IRepository<T>
    {
        public IEnumerable<T> Objects { get; set; }

        public FakeRepository(IEnumerable<T> expected = null)
        {
            Objects = expected;
        }

        public IQueryable<T> GetAll()
        {
            return Objects == null ? Enumerable.Empty<T>().AsQueryable() : Objects.AsQueryable();
        }
    }
}

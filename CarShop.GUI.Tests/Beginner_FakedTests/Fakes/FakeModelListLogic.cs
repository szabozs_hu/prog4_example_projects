﻿using CarShop.BL;
using CarShop.BL.Interfaces;
using CarShop.Data;
using System;
using System.Collections.Generic;

namespace CarShop.GUI.Tests.Beginner_FakedTests.Fakes
{
    /// <summary>
    /// This is an IModelListLogic implementor without any meaningful logic, only for testing purposes. 
    /// </summary>
    internal class FakeModelListLogic : IModelListLogic
    {
        public FilterSet FilterSetOfLastCall { get; set; }
        public IList<Model> SavedModels { get; } = new List<Model>();
        public IList<Tuple<int, BaseModel, IList<Extra>>> ThrowCalls { get; } = new List<Tuple<int, BaseModel, IList<Extra>>>();
        public bool Throws { get; set; }

        public event EventHandler ModelListChanged = null;

        public void Delete(Model model)
        {
            throw new NotImplementedException();
        }

        public IList<Model> GetFilteredModelList(FilterSet filters)
        {
            FilterSetOfLastCall = filters;
            return new List<Model>();
        }

        public void Save(Model model)
        {
            SavedModels.Add(model);
        }

        public void ThrowIfExists(int id, BaseModel baseModel, IList<Extra> extras)
        {
            ThrowCalls.Add(new Tuple<int, BaseModel, IList<Extra>>(id, baseModel, extras));
            if (Throws)
                throw new Exception();
        }

        public void FireEvent()
        {
            ModelListChanged(this, EventArgs.Empty);
        }
    }
}

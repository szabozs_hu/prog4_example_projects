﻿using CarShop.BL.Interfaces;
using CarShop.Data;
using System;
using System.Collections.Generic;

namespace CarShop.GUI.Tests.Beginner_FakedTests.Fakes
{
    internal class FakeModelPriceLogic : IModelPriceLogic
    {
        public decimal ExpectedPrice { get; set; }

        public Tuple<BaseModel, IEnumerable<Extra>> ExpectedPriceCallParams { get; set; }

        public decimal CalculatePrice(Model model)
        {
            return ExpectedPrice;
        }

        public decimal CalculatePrice(BaseModel baseModel, IEnumerable<Extra> extras)
        {
            ExpectedPriceCallParams = new Tuple<BaseModel, IEnumerable<Extra>>(baseModel, extras);
            return ExpectedPrice;
        }
    }
}

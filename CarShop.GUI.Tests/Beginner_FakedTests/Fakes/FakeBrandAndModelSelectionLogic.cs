﻿using CarShop.BL.Interfaces;
using CarShop.Data;
using System.Collections.Generic;

namespace CarShop.GUI.Tests.Beginner_FakedTests.Fakes
{
    /// <summary>
    /// This is an IBrandAndModelSelectionLogic implementor, only for testing. 
    /// </summary>
    internal class FakeBrandAndModelSelectionLogic : IBrandAndBaseModelSelectionLogic
    {
        private IList<Brand> brands;
        private IDictionary<Brand, IList<BaseModel>> modelsOfBrands = new Dictionary<Brand, IList<BaseModel>>();
        private IList<BaseModel> modelsForAnyBrand;

        public int GetSelectableModelsCalls { get; set; } = 0;

        public void SetupModelsForAnyBrand(IList<BaseModel> baseModels)
        {
            modelsForAnyBrand = baseModels;
        }

        public void SetupModelsOfBrand(Brand brand, IList<BaseModel> baseModels)
        {
            modelsOfBrands[brand] = baseModels;
        }

        public void SetupBrands(IList<Brand> brands)
        {
            this.brands = brands;
        }

        public IList<Brand> GetSelectableBrands(BaseModel selected)
        {
            return brands;
        }

        public IList<BaseModel> GetSelectableBaseModels(Brand selected)
        {
            GetSelectableModelsCalls++;
            if (selected != null && modelsOfBrands.ContainsKey(selected))
            {
                return modelsOfBrands[selected];
            }
            else
            {
                return modelsForAnyBrand;
            }
        }
    }
}

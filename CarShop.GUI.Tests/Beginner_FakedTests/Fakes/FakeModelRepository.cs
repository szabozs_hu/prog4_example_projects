﻿using CarShop.Data;
using CarShop.Data.Interfaces;
using System;
using System.Collections.Generic;

namespace CarShop.GUI.Tests.Beginner_FakedTests.Fakes
{
    /// <summary>
    /// This is an IModelRepository implementor without any meaningful logic, only for testing purposes. 
    /// </summary>
    internal class FakeModelRepository : FakeRepository<Model>, IModelRepository
    {
        public IList<Model> DeletedObjects { get; } = new List<Model>();
        public IList<Model> SavedObjects { get; } = new List<Model>();
        public IList<Tuple<int, BaseModel, IList<Extra>>> ThrowCalls { get; } = new List<Tuple<int, BaseModel, IList<Extra>>>();

        public FakeModelRepository(IEnumerable<Model> expectedModels = null)
            : base(expectedModels)
        {
        }

        public void Delete(Model model)
        {
            DeletedObjects.Add(model);
        }

        public void Save(Model model)
        {
            SavedObjects.Add(model);
        }

        public void ThrowIfExists(int id, BaseModel baseModel, IList<Extra> extras)
        {
            ThrowCalls.Add(new Tuple<int, BaseModel, IList<Extra>>(id, baseModel, extras));
        }
    }
}

﻿using CarShop.Data;
using CarShop.GUI.Tests.Beginner_FakedTests.Fakes;
using NUnit.Framework;

namespace CarShop.GUI.Tests.Beginner_FakedTests
{
    /// <summary>
    /// Tests for ModelListWindowViewModel
    /// </summary>
    [TestFixture]
    internal class ModelListWindowViewModelTests_WithFakes
    {
        [Test]
        public void WhenGettingModels_ReturnsModelListFromRepository()
        {
            // ARRANGE 
            var expectedModels = new[] { new BaseModel(), new BaseModel() };

            var modelRepository = new FakeRepository<BaseModel>();
            modelRepository.Objects = expectedModels;

            var viewModel = new BaseModelListWindowViewModel(modelRepository);

            // ACT 
            var models = viewModel.BaseModels;

            // ASSERT 
            Assert.That(models, Is.EquivalentTo(expectedModels));
        }
    }
}

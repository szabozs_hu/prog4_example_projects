﻿using CarShop.Data;
using CarShop.GUI.Tests.Beginner_FakedTests.Fakes;
using NUnit.Framework;

namespace CarShop.GUI.Tests.Beginner_FakedTests
{
    /// <summary>
    /// Tests for ExtraListWindowViewModel
    /// </summary>
    [TestFixture]
    internal class ExtraListWindowViewModelTests_WithFakes
    {
        [Test]
        public void WhenGettingExtras_ReturnsExtraListFromRepository()
        {
            // ARRANGE 
            var expectedExtras = new[] { new Extra(), new Extra() };

            var extraRepository = new FakeRepository<Extra>();
            extraRepository.Objects = expectedExtras;

            var viewModel = new ExtraListWindowViewModel(extraRepository);

            // ACT 
            var extras = viewModel.Extras;

            // ASSERT 
            Assert.That(extras, Is.EquivalentTo(expectedExtras));
        }
    }
}

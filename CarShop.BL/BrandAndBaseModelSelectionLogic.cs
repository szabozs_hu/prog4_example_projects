﻿using CarShop.BL.Interfaces;
using CarShop.Data;
using CarShop.Data.Interfaces;
using System.Collections.Generic;

namespace CarShop.BL
{
    ///  <inheritdoc />
    public class BrandAndBaseModelSelectionLogic : IBrandAndBaseModelSelectionLogic
    {
        private readonly IRepository<Brand> brandRepository;
        private readonly IRepository<BaseModel> baseModelRepository;

        /// <inheritdoc /> 
        public BrandAndBaseModelSelectionLogic(
            IRepository<Brand> brandRepository,
            IRepository<BaseModel> baseModelRepository)
        {
            this.brandRepository = brandRepository;
            this.baseModelRepository = baseModelRepository;
        }

        ///  <inheritdoc />
        public IList<Brand> GetSelectableBrands(BaseModel selected)
        {
            List<Brand> selectableBrands = new List<Brand>();

            selectableBrands.AddRange(brandRepository.GetAll());

            return selectableBrands;
        }

        ///  <inheritdoc />
        public IList<BaseModel> GetSelectableBaseModels(Brand selected)
        {
            List<BaseModel> selectableModels = new List<BaseModel>();

            if (selected == null)
            {
                selectableModels.AddRange(baseModelRepository.GetAll());
            }
            else
            {
                selectableModels.AddRange(selected.BaseModels);
            }

            return selectableModels;
        }
    }
}

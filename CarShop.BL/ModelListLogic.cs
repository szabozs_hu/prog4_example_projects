﻿using CarShop.BL.Interfaces;
using CarShop.Data;
using CarShop.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarShop.BL
{
    /// <inheritdoc /> 
    public class ModelListLogic : IModelListLogic
    {
        private readonly IModelRepository modelRepository;
        private readonly IModelPriceLogic modelPriceLogic;

        /// <inheritdoc /> 
        public ModelListLogic(IModelRepository modelRepository, IModelPriceLogic modelPriceLogic)
        {
            this.modelRepository = modelRepository;
            this.modelPriceLogic = modelPriceLogic;
        }

        /// Note that any kind of event would do. We could also implement / fire CollectionChanged 
        /// here instead of this. The only thing that matters that the outside is notified about any 
        /// collection change manually - we must do this manually because the DbSet itself fires no event. 
        /// Another possibility is binding to the DBSet's .Local property, then inmemory filtering 
        /// must be used in the listbox.
        public event EventHandler ModelListChanged;

        /// <inheritdoc /> 
        public IList<Model> GetFilteredModelList(FilterSet filters)
        {
            var models = modelRepository.GetAll();

            if (filters.BrandFilter != null)
            {
                models = models.Where(m => m.BaseModel.Brand.Name.StartsWith(filters.BrandFilter));
            }

            if (filters.BaseModelFilter != null)
            {
                models = models.Where(m => m.BaseModel.Name.StartsWith(filters.BaseModelFilter));
            }

            if (filters.MinPriceFilter != uint.MinValue || filters.MaxPriceFilter != uint.MaxValue)
            {
                // in-memory filtering for prices because of CalculatePrice - a stored procedure would be far better 
                return models.ToList()
                    .Where(m =>
                         modelPriceLogic.CalculatePrice(m) >= filters.MinPriceFilter &&
                         modelPriceLogic.CalculatePrice(m) <= filters.MaxPriceFilter).ToList();
            }

            return models.ToList();
        }

        /// <inheritdoc /> 
        public void Delete(Model model)
        {
            try
            {
                modelRepository.Delete(model);
            }
            finally
            {
                OnModelListChanged();
            }
        }

        /// <inheritdoc /> 
        public void Save(Model model)
        {
            try
            {
                modelRepository.Save(model);
            }
            finally
            {
                OnModelListChanged();
            }
        }

        public void ThrowIfExists(int id, BaseModel baseModel, IList<Extra> extras)
        {
            modelRepository.ThrowIfExists(id, baseModel, extras);
        }

        private void OnModelListChanged()
        {
            ModelListChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}

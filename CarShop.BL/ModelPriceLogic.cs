﻿using CarShop.BL.Interfaces;
using CarShop.Data;
using System.Collections.Generic;
using System.Linq;

namespace CarShop.BL
{
    /// <inheritdoc /> 
    public class ModelPriceLogic : IModelPriceLogic
    {
        /// <inheritdoc /> 
        public decimal CalculatePrice(Model model)
        {
            return CalculatePrice(model.BaseModel, model.Extras);
        }

        /// <inheritdoc /> 
        public decimal CalculatePrice(BaseModel baseModel, IEnumerable<Extra> extras)
        {
            return extras.Sum(e => e.Price) + baseModel.Price;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using CarShop.Data;

namespace CarShop.BL.Interfaces
{
    /// <summary>
    /// Business logic for filtering the model list 
    /// </summary>
    public interface IModelListLogic
    {
        /// <summary>
        /// Gets a filtered model collection
        /// </summary>
        IList<Model> GetFilteredModelList(FilterSet filters);

        /// <summary>
        /// Deletes model from the model list 
        /// </summary>
        void Delete(Model model);

        /// <summary>
        /// Saves model regardless of it is already in the modellist or not 
        /// </summary>
        void Save(Model model);

        /// <summary>
        /// Checks whether a model with the given setting exists in the model list. 
        /// </summary>
        void ThrowIfExists(int id, BaseModel baseModel, IList<Extra> extras);

        /// <summary>
        /// Is fired when a delete or save happens 
        /// </summary>
        event EventHandler ModelListChanged;
    }
}
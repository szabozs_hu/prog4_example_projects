﻿using CarShop.Data;
using System.Collections.Generic;

namespace CarShop.BL.Interfaces
{
    /// <summary>
    /// Business logic for calculating the price of a model 
    /// </summary>
    public interface IModelPriceLogic
    {
        /// <summary>
        /// Calculates the price based on the BaseModel price and the extras.
        /// </summary>
        decimal CalculatePrice(Model model);

        /// <summary>
        /// Calculates the price based on the BaseModel price and the extras.
        /// </summary>
        decimal CalculatePrice(BaseModel baseModel, IEnumerable<Extra> extras);
    }
}
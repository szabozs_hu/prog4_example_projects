﻿using CarShop.Data;
using System.Collections.Generic;

namespace CarShop.BL.Interfaces
{
    /// <summary>
    /// Business logic for getting models that are selectable if a Brand is selected; 
    /// or selectable brands when a model is selected. 
    /// </summary>
    public interface IBrandAndBaseModelSelectionLogic
    {
        /// <summary>
        /// Get selectable models if the user has selected the brand 
        /// </summary>
        IList<BaseModel> GetSelectableBaseModels(Brand selected);

        /// <summary>
        /// Get selectable brands if the user has selected the model 
        /// </summary>
        IList<Brand> GetSelectableBrands(BaseModel selected);
    }
}

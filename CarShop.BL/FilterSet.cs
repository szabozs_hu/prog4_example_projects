﻿namespace CarShop.BL
{
    /// <summary>
    /// Class that contains a set of filters 
    /// </summary>
    public class FilterSet
    {   
        /// <summary>
        /// Brand name. 
        /// </summary>
        public string BrandFilter { get; set; }
        
        /// <summary>
        /// Model name. 
        /// </summary>
        public string BaseModelFilter { get; set; }

        /// <summary>
        /// Min price.  
        /// </summary>
        public uint MinPriceFilter { get; set; } 

        /// <summary>
        /// Max price. 
        /// </summary>
        public uint MaxPriceFilter { get; set; } 
        
        /// <summary>
        /// Creates a filterset.
        /// </summary>
        public FilterSet(string brandFilter, string modelFilter, uint? minPriceFilter, uint? maxPriceFilter)
        {
            this.BrandFilter = string.IsNullOrEmpty(brandFilter) ? null : brandFilter;
            this.BaseModelFilter = string.IsNullOrEmpty(modelFilter) ? null : modelFilter;
            this.MinPriceFilter = minPriceFilter ?? uint.MinValue;
            this.MaxPriceFilter = maxPriceFilter ?? uint.MaxValue;
        }
    }
}

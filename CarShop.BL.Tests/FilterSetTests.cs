﻿using NUnit.Framework;

namespace CarShop.BL.Tests
{
    /// <summary>
    /// Tests for FilterSet
    /// </summary>
    [TestFixture]
    public class FilterSetTests
    {
        [Test]
        public void WhenBrandFilterIsEmptyString_ItIsSubstitutedWithNull()
        {
            // ARRANGE - ACT 
            FilterSet set = new FilterSet(string.Empty, "Com", 100, 500);

            // ASSERT 
            Assert.That(set.BrandFilter, Is.Null);
            Assert.That(set.BaseModelFilter, Is.EqualTo("Com"));
            Assert.That(set.MinPriceFilter, Is.EqualTo(100));
            Assert.That(set.MaxPriceFilter, Is.EqualTo(500));
        }

        [Test]
        public void WhenModelFilterIsEmptyString_ItIsSubstitutedWithNull()
        {
            // ARRANGE - ACT 
            FilterSet set = new FilterSet("Op", string.Empty, 100, 500);

            // ASSERT 
            Assert.That(set.BrandFilter, Is.EqualTo("Op"));
            Assert.That(set.BaseModelFilter, Is.Null);
            Assert.That(set.MinPriceFilter, Is.EqualTo(100));
            Assert.That(set.MaxPriceFilter, Is.EqualTo(500));
        }

        [Test]
        public void WhenMinPriceFilterIsNull_ItIsSubstitutedWithMinValue()
        {
            // ARRANGE - ACT 
            FilterSet set = new FilterSet("Op", "Com", null, 500);

            // ASSERT 
            Assert.That(set.BrandFilter, Is.EqualTo("Op"));
            Assert.That(set.BaseModelFilter, Is.EqualTo("Com"));
            Assert.That(set.MinPriceFilter, Is.EqualTo(uint.MinValue));
            Assert.That(set.MaxPriceFilter, Is.EqualTo(500));
        }

        [Test]
        public void WhenMaxPriceFilterIsNull_ItIsSubstitutedWithMaxValue()
        {
            // ARRANGE - ACT 
            FilterSet set = new FilterSet("Op", "Com", 100, null);

            // ASSERT 
            Assert.That(set.BrandFilter, Is.EqualTo("Op"));
            Assert.That(set.BaseModelFilter, Is.EqualTo("Com"));
            Assert.That(set.MinPriceFilter, Is.EqualTo(100));
            Assert.That(set.MaxPriceFilter, Is.EqualTo(uint.MaxValue));
        }
    }
}

﻿using CarShop.BL.Interfaces;
using CarShop.Data;
using CarShop.Data.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace CarShop.BL.Tests.Pro_MockedTests
{
    /// <summary>
    /// Tests for ModelListLogicTests
    /// </summary>
    [TestFixture]
    internal class ModelListLogicTests_WithMocks
    {
        private Mock<IModelRepository> modelRepositoryMock;
        private Mock<IModelPriceLogic> modelPriceLogicMock;

        private Model[] testModels;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var brand1 = new Brand() { Name = "ABrand" };
            var brand2 = new Brand() { Name = "BBrand" };

            var baseModel1 = new BaseModel() { Brand = brand1, Name = "Model1xxx" };
            var baseModel2 = new BaseModel() { Brand = brand1, Name = "Model2xxx" };
            var baseModel3 = new BaseModel() { Brand = brand2, Name = "Model1xxx" };

            testModels = new[]
            {
                new Model() { BaseModel = baseModel1 },
                new Model() { BaseModel = baseModel1 },
                new Model() { BaseModel = baseModel2 },
                new Model() { BaseModel = baseModel2 },
                new Model() { BaseModel = baseModel3 },
                new Model() { BaseModel = baseModel3 },
            };
        }

        [SetUp]
        public void Setup()
        {
            modelPriceLogicMock = new Mock<IModelPriceLogic>();

            modelRepositoryMock = new Mock<IModelRepository>();
            modelRepositoryMock.Setup(m => m.GetAll()).Returns(testModels.AsQueryable());
        }

        [Test]
        public void WhenCallingGetFilteredModelListWithBrandFilter_ReturnsCorrectModels()
        {
            // ARRANGE 
            var filterSet = new FilterSet("ABra", null, null, null);
            var modelListLogic = new ModelListLogic(modelRepositoryMock.Object, modelPriceLogicMock.Object);

            // ACT 
            var models = modelListLogic.GetFilteredModelList(filterSet).ToList();

            // ASSERT 
            Assert.That(models, Is.EquivalentTo(testModels.Take(4)));
        }

        [Test]
        public void WhenCallingGetFilteredModelListWithModelFilter_ReturnsCorrectModels()
        {
            // ARRANGE 
            var filterSet = new FilterSet(null, "Model1", null, null);
            var modelListLogic = new ModelListLogic(modelRepositoryMock.Object, modelPriceLogicMock.Object);

            // ACT 
            var models = modelListLogic.GetFilteredModelList(filterSet).ToList();

            // ASSERT 
            Assert.That(models, Is.EquivalentTo(TakeTestModels(0, 1, 4, 5)));
        }

        [Test]
        public void WhenCallingGetFilteredModelListWithMinMaxFilter_ReturnsCorrectModels()
        {
            // ARRANGE 
            for (int i = 0; i < testModels.Length; i++)
            {
                modelPriceLogicMock.Setup(m => m.CalculatePrice(testModels[i])).Returns(i * 10);
            }

            var filterSet = new FilterSet(null, null, 20, 40);
            var modelListLogic = new ModelListLogic(modelRepositoryMock.Object, modelPriceLogicMock.Object);

            // ACT 
            var models = modelListLogic.GetFilteredModelList(filterSet).ToList();

            // ASSERT 
            Assert.That(models, Is.EquivalentTo(TakeTestModels(2, 3, 4)));
        }

        [Test]
        public void WhenCallingDelete_ForwardsToRepository()
        {
            // ARRANGE 
            modelRepositoryMock.Setup(m => m.Delete(It.IsAny<Model>()));
            var modelListLogic = new ModelListLogic(modelRepositoryMock.Object, modelPriceLogicMock.Object);

            // ACT 
            modelListLogic.Delete(testModels[0]);

            // ASSERT 
            modelRepositoryMock.Verify(m => m.Delete(testModels[0]), Times.Once);
        }

        [Test]
        public void WhenCallingDelete_ListChangeIsFired()
        {
            // ARRANGE 
            int calls = 0;
            var modelListLogic = new ModelListLogic(modelRepositoryMock.Object, modelPriceLogicMock.Object);
            modelListLogic.ModelListChanged += (s, e) => calls++;

            // ACT 
            modelListLogic.Delete(testModels[0]);

            // ASSERT 
            Assert.That(calls, Is.EqualTo(1));
        }

        [Test]
        public void WhenCallingSave_ForwardsToRepository()
        {
            // ARRANGE 
            modelRepositoryMock.Setup(m => m.Save(It.IsAny<Model>()));
            var modelListLogic = new ModelListLogic(modelRepositoryMock.Object, modelPriceLogicMock.Object);

            // ACT 
            modelListLogic.Save(testModels[0]);

            // ASSERT 
            modelRepositoryMock.Verify(m => m.Save(testModels[0]), Times.Once);
        }

        [Test]
        public void WhenCallingSave_ListChangeIsFired()
        {
            // ARRANGE 
            int calls = 0;
            var modelListLogic = new ModelListLogic(modelRepositoryMock.Object, modelPriceLogicMock.Object);
            modelListLogic.ModelListChanged += (s, e) => calls++;

            // ACT 
            modelListLogic.Save(testModels[0]);

            // ASSERT 
            Assert.That(calls, Is.EqualTo(1));
        }

        [Test]
        public void WhenCallingThrowIfThrowIfExists_ForwardsToRepository()
        {
            // ARRANGE 
            modelRepositoryMock.Setup(m => m.ThrowIfExists(It.IsAny<int>(), It.IsAny<BaseModel>(), It.IsAny<IList<Extra>>()));

            var modelListLogic = new ModelListLogic(modelRepositoryMock.Object, modelPriceLogicMock.Object);

            var extras = new[] 
            {
                new Extra() { Id = 7 },
                new Extra() { Id = 8 }
            };

            // ACT 
            modelListLogic.ThrowIfExists(6, testModels[2].BaseModel, extras);

            // ASSERT 
            modelRepositoryMock.Verify(m => m.ThrowIfExists(6, testModels[2].BaseModel, extras), Times.Once);
        }

        private IList<Model> TakeTestModels(params int[] indices)
        {
            List<Model> results = new List<Model>();

            foreach (int i in indices)
            {
                results.Add(testModels[i]);
            }

            return results;
        }
    }
}

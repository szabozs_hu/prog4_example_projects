﻿using CarShop.Data;
using CarShop.Data.Interfaces;
using Moq;
using NUnit.Framework;
using System.Linq;

namespace CarShop.BL.Tests.Pro_MockedTests
{
    /// <summary>
    /// Tests for BrandAndModelSelectionLogicTests
    /// </summary>
    [TestFixture]
    internal class BrandAndModelSelectionLogicTests_WithMocks
    {
        private Brand[] brands;
        private BaseModel[] baseModels;

        private BrandAndBaseModelSelectionLogic selectionLogic;

        [OneTimeSetUp]
        public void Setup()
        {
            CreateBrandAndModelArray();

            var brandRepositoryMock = new Mock<IRepository<Brand>>();

            brandRepositoryMock
                .Setup(m => m.GetAll())
                .Returns(brands.AsQueryable());

            var modelRepositoryMock = new Mock<IRepository<BaseModel>>();
            modelRepositoryMock
                .Setup(m => m.GetAll())
                .Returns(baseModels.AsQueryable());

            selectionLogic = new BrandAndBaseModelSelectionLogic(brandRepositoryMock.Object, modelRepositoryMock.Object);
        }

        private void CreateBrandAndModelArray()
        {
            brands = new[]
                            {
                    new Brand { Name = "ABrand" },
                    new Brand { Name = "BBrand" }
                };

            baseModels = new[]
                    {
                        new BaseModel { Name = "ABrand_AModel" },
                        new BaseModel { Name = "ABrand_BModel" },
                        new BaseModel { Name = "BBrand_AModel" }
                    };

            Connect(brands[0], baseModels[0]);
            Connect(brands[0], baseModels[1]);
            Connect(brands[1], baseModels[2]);
        }

        [Test]
        public void WhenTheUserHasSelectedAModel_AnyBrandCanBeSelected()
        {
            // ARRANGE 
            var selectedModel = baseModels.Single(m => m.Name == "ABrand_AModel");

            // ACT 
            var selectableBrands = selectionLogic.GetSelectableBrands(selectedModel).ToList();

            // ASSERT 
            Assert.That(selectableBrands, Is.EquivalentTo(brands));
        }

        [Test]
        public void WhenTheUserHasNotSelectedAModel_AnyBrandCanBeSelected()
        {
            // ACT 
            var selectableBrands = selectionLogic.GetSelectableBrands(null).ToList();

            // ASSERT 
            Assert.That(selectableBrands, Is.EquivalentTo(brands));
        }

        [Test]
        public void WhenTheUserHasSelectedABrand_OnlyModelsFromThatBrandCanBeSelected()
        {
            // ARRANGE 
            var selectedBrand = brands.Single(m => m.Name == "ABrand");

            // ACT 
            var selectableModels = selectionLogic.GetSelectableBaseModels(selectedBrand).ToList();

            // ASSERT 
            Assert.That(selectableModels.Count, Is.EqualTo(2));
            Assert.That(selectableModels[0].Name, Is.EqualTo("ABrand_AModel"));
            Assert.That(selectableModels[1].Name, Is.EqualTo("ABrand_BModel"));
        }

        [Test]
        public void WhenTheUserHasNotSelectedABrand_AnyModelCanBeSelected()
        {
            // ACT 
            var selectableModels = selectionLogic.GetSelectableBaseModels(null).ToList();

            // ASSERT 
            Assert.That(selectableModels, Is.EquivalentTo(baseModels));
        }

        private void Connect(Brand brand, BaseModel model)
        {
            brand.BaseModels.Add(model);
            model.Brand = brand;
        }
    }
}

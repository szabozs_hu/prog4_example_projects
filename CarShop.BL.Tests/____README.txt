﻿This project contains 2 kinds of tests for the functionality in the BL code, for demonstration purposes.
YOU DON'T NEED TO WRITE BOTH KINDS OF TESTS in your own project :) - use the variant that is closer to your thinking,
or more useful in the particular test you want to write. 
You can also use mixed mocking & faking tests if that fits your case best.

Beginner_FakedTests:
These are written without mocks. You can use these if your course had only 12 weeks in the semester 
and the material about mocking was omitted. 

Pro_MockedTests:
These are written with Moq mocks. 

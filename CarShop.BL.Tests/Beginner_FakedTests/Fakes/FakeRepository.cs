﻿using CarShop.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace CarShop.BL.Tests.Beginner_FakedTests.Fakes
{
    /// <summary>
    /// This is a repository implementor without any meaningful logic, to be used in tests. 
    /// </summary>
    internal class FakeRepository<T> : IRepository<T>
    {
        private IEnumerable<T> expected;

        public FakeRepository(IEnumerable<T> expected)
        {
            this.expected = expected;
        }

        public IQueryable<T> GetAll()
        {
            return expected.AsQueryable();
        }
    }
}

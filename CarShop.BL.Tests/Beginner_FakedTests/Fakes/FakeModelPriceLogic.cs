﻿using CarShop.BL.Interfaces;
using CarShop.Data;
using System.Collections.Generic;

namespace CarShop.BL.Tests.Beginner_FakedTests.Fakes
{
    /// <summary>
    /// This is an IModelPriceLogic implementor without any meaningful logic, only for testing purposes. 
    /// </summary>
    internal class FakeModelPriceLogic : IModelPriceLogic
    {
        private Dictionary<Model, decimal> expectedPrices = new Dictionary<Model, decimal>();

        public void Setup(Model model, decimal price)
        {
            expectedPrices[model] = price;
        }

        public decimal CalculatePrice(Model model)
        {
            return expectedPrices[model];
        }

        public decimal CalculatePrice(BaseModel baseModel, IEnumerable<Extra> extras)
        {
            return 0;
        }
    }
}

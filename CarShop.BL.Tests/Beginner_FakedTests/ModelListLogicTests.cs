﻿using CarShop.BL.Tests.Beginner_FakedTests.Fakes;
using CarShop.Data;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace CarShop.BL.Tests.Beginner_FakedTests
{
    /// <summary>
    /// Tests for ModelListLogicTests
    /// </summary>
    [TestFixture]
    internal class ModelListLogicTests_WithFakes
    {
        private FakeModelRepository modelRepository;
        private FakeModelPriceLogic modelPriceLogic;

        private Model[] testModels;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var brand1 = new Brand() { Name = "ABrand" };
            var brand2 = new Brand() { Name = "BBrand" };

            var baseModel1 = new BaseModel() { Brand = brand1, Name = "Model1xxx" };
            var baseModel2 = new BaseModel() { Brand = brand1, Name = "Model2xxx" };
            var baseModel3 = new BaseModel() { Brand = brand2, Name = "Model1xxx" };

            testModels = new[]
            {
                new Model() { BaseModel = baseModel1 },
                new Model() { BaseModel = baseModel1 },
                new Model() { BaseModel = baseModel2 },
                new Model() { BaseModel = baseModel2 },
                new Model() { BaseModel = baseModel3 },
                new Model() { BaseModel = baseModel3 },
            };
        }

        [SetUp]
        public void Setup()
        {
            modelPriceLogic = new FakeModelPriceLogic();
            modelRepository = new FakeModelRepository(testModels);
        }

        [Test]
        public void WhenCallingGetFilteredModelListWithBrandFilter_ReturnsCorrectModels()
        {
            // ARRANGE 
            var filterSet = new FilterSet("ABra", null, null, null);
            var modelListLogic = new ModelListLogic(modelRepository, modelPriceLogic);

            // ACT 
            var models = modelListLogic.GetFilteredModelList(filterSet).ToList();

            // ASSERT 
            Assert.That(models, Is.EquivalentTo(testModels.Take(4)));
        }

        [Test]
        public void WhenCallingGetFilteredModelListWithModelFilter_ReturnsCorrectModels()
        {
            // ARRANGE 
            var filterSet = new FilterSet(null, "Model1", null, null);
            var modelListLogic = new ModelListLogic(modelRepository, modelPriceLogic);

            // ACT 
            var models = modelListLogic.GetFilteredModelList(filterSet).ToList();

            // ASSERT 
            Assert.That(models, Is.EquivalentTo(TakeTestModels(0, 1, 4, 5)));
        }

        [Test]
        public void WhenCallingGetFilteredModelListWithMinMaxFilter_ReturnsCorrectModels()
        {
            // ARRANGE 
            for (int i = 0; i < testModels.Length; i++)
            {
                modelPriceLogic.Setup(testModels[i], i * 10);
            }

            var filterSet = new FilterSet(null, null, 20, 40);
            var modelListLogic = new ModelListLogic(modelRepository, modelPriceLogic);

            // ACT 
            var models = modelListLogic.GetFilteredModelList(filterSet).ToList();

            // ASSERT 
            Assert.That(models, Is.EquivalentTo(TakeTestModels(2, 3, 4)));
        }

        [Test]
        public void WhenCallingDelete_ForwardsToRepository()
        {
            // ARRANGE 
            var modelListLogic = new ModelListLogic(modelRepository, modelPriceLogic);

            // ACT 
            modelListLogic.Delete(testModels[0]);

            // ASSERT 
            Assert.That(modelRepository.DeletedObjects.Count, Is.EqualTo(1));
            Assert.That(modelRepository.DeletedObjects[0], Is.EqualTo(testModels[0]));
        }

        [Test]
        public void WhenCallingDelete_ListChangeIsFired()
        {
            // ARRANGE 
            int calls = 0;
            var modelListLogic = new ModelListLogic(modelRepository, modelPriceLogic);
            modelListLogic.ModelListChanged += (s, e) => calls++;

            // ACT 
            modelListLogic.Delete(testModels[0]);

            // ASSERT 
            Assert.That(calls, Is.EqualTo(1));
        }

        [Test]
        public void WhenCallingSave_ForwardsToRepository()
        {
            // ARRANGE 
            var modelListLogic = new ModelListLogic(modelRepository, modelPriceLogic);

            // ACT 
            modelListLogic.Save(testModels[0]);

            // ASSERT 
            Assert.That(modelRepository.SavedObjects.Count, Is.EqualTo(1));
            Assert.That(modelRepository.SavedObjects[0], Is.EqualTo(testModels[0]));
        }

        [Test]
        public void WhenCallingSave_ListChangeIsFired()
        {
            // ARRANGE 
            int calls = 0;
            var modelListLogic = new ModelListLogic(modelRepository, modelPriceLogic);
            modelListLogic.ModelListChanged += (s, e) => calls++;

            // ACT 
            modelListLogic.Save(testModels[0]);

            // ASSERT 
            Assert.That(calls, Is.EqualTo(1));
        }

        [Test]
        public void WhenCallingThrowIfThrowIfExists_ForwardsToRepository()
        {
            // ARRANGE 
            var modelListLogic = new ModelListLogic(modelRepository, modelPriceLogic);

            var extras = new[] 
            {
                new Extra() { Id = 7 },
                new Extra() { Id = 8 }
            };

            // ACT 
            modelListLogic.ThrowIfExists(6, testModels[2].BaseModel, extras);

            // ASSERT 
            Assert.That(modelRepository.ThrowCalls.Count, Is.EqualTo(1));
            Assert.That(modelRepository.ThrowCalls[0].Item1, Is.EqualTo(6));
            Assert.That(modelRepository.ThrowCalls[0].Item2, Is.EqualTo(testModels[2].BaseModel));
            Assert.That(modelRepository.ThrowCalls[0].Item3, Is.EquivalentTo(extras));
        }

        private IList<Model> TakeTestModels(params int[] indices)
        {
            List<Model> results = new List<Model>();

            foreach (int i in indices)
            {
                results.Add(testModels[i]);
            }

            return results;
        }
    }
}

﻿using CarShop.Data;
using NUnit.Framework;

namespace CarShop.BL.Tests
{
    /// <summary>
    /// Tests for ModelPriceLogic
    /// </summary>
    [TestFixture]
    internal class ModelPriceLogicTests
    {
        [Test]
        public void WhenCallingCalculatePrice_ReturnsFullPriceFromBaseModelAndExtras()
        {
            // ARRANGE 
            BaseModel baseModel = new BaseModel() { Price = 10000 };
            Extra extra1 = new Extra() { Price = 200 };
            Extra extra2 = new Extra() { Price = 600 };

            Model model = new Model()
            {
                BaseModel = baseModel,
                Extras = new[] { extra1, extra2 }
            };

            ModelPriceLogic logic = new ModelPriceLogic();

            // ACT 
            var price = logic.CalculatePrice(model);

            // ASSERT
            Assert.That(price, Is.EqualTo(10800));
        }
    }
}
